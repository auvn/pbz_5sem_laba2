package com.auvn.pbz2_app.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBAdaptor {
	// private Object[] columns;
	// private Object[][] data;
	private DBConnector dbConnector;

	public DBAdaptor(DBConnector dbConnector) {
		this.dbConnector = dbConnector;
	}

	public Object[] getColumns(ResultSet resultSet) throws SQLException {
		int columnCount = resultSet.getMetaData().getColumnCount();
		Object[] columns = new Object[columnCount];
		for (int i = 1; i <= columnCount; i++) {
			columns[i - 1] = resultSet.getMetaData().getColumnName(i);
		}

		return columns;
	}

	public Object[][] getData(ResultSet resultSet) throws SQLException {
		int columnCount = resultSet.getMetaData().getColumnCount();
		ArrayList<Object[]> tempArrayList = new ArrayList<Object[]>(columnCount);
		Object[] row;
		while (resultSet.next()) {
			row = new Object[columnCount];
			for (int i = 1; i <= columnCount; i++) {
				row[i - 1] = resultSet.getObject(i);
			}
			tempArrayList.add(row);
		}
		Object[][] data = new Object[tempArrayList.size()][];
		for (int i = 0; i < tempArrayList.size(); i++)
			data[i] = tempArrayList.get(i);

		return data;
	}

	public ResultSet task_1() throws SQLException {
		return dbConnector.executeQuery("call task_1()");
	}

	public ResultSet task_3(int id, int rockAltitude, int countryId)
			throws SQLException {
		return dbConnector.executeQuery("call task_3(" + id + ","
				+ rockAltitude + "," + countryId + ")");
	}

	public ResultSet task_4(String eventStartDate, String eventStopDate)
			throws SQLException {
		return dbConnector.executeQuery("call task_4('" + eventStartDate
				+ "','" + eventStopDate + "')");
	}

	public ResultSet task_6() throws SQLException {
		return dbConnector.executeQuery("call task_6()");
	}

	public ResultSet task_7(String eventStartDate, String eventStopDate)
			throws SQLException {
		return dbConnector.executeQuery("call task_7('" + eventStartDate
				+ "','" + eventStopDate + "')");
	}

	public ResultSet task_9() throws SQLException {
		return dbConnector.executeQuery("call task_9()");
	}

	public int getAddressId(String addressName) throws SQLException {
		ResultSet resultSet = dbConnector.executeQuery(
				"select address_id from " + DBTables.DB_ADDRESSES_TABLE
						+ " where address_info=?", addressName);
		if (resultSet.next())
			return resultSet.getInt(1);
		return -1;
	}

	public int addAddressRow(String addressName) throws SQLException {
		int id = getAddressId(addressName);
		if (id == -1) {
			dbConnector.executeQuery("call add_address(?)", addressName);
			return getAddressId(addressName);
		} else
			return id;
	}

	public int addCountryRow(String countryName) throws SQLException {
		int id = getCountryId(countryName);
		if (id == -1) {
			dbConnector.executeQuery("call add_country(?)", countryName);
			return getCountryId(countryName);
		} else
			return id;

	}

	public int addEventRow(String eventName, String eventStartDate,
			String eventStopDate, String eventRockName) throws SQLException {
		int id = getEventId(eventName);
		if (id == -1) {
			dbConnector.executeQuery("call add_event('" + eventName + "','"
					+ eventStartDate + "','" + eventStopDate + "',"
					+ getRockId(eventRockName) + ")");
			return getEventId(eventName);
		} else
			return id;
	}

	public int getCountryId(String countryName) throws SQLException {
		ResultSet resultSet = dbConnector.executeQuery(
				"select country_id from " + DBTables.DB_COUNTRIES_TABLE
						+ " where country_name=?", countryName);
		if (resultSet.next())
			return resultSet.getInt(1);
		return -1;
	}

	public int getRockId(String rockName) throws SQLException {
		ResultSet resultSet = dbConnector.executeQuery("select rock_id from "
				+ DBTables.DB_ROCKS_TABLE + " where rock_name=?", rockName);
		if (resultSet.next())
			return resultSet.getInt(1);
		return -1;
	}

	public int getAlpinistId(String alpinistName) throws SQLException {
		ResultSet resultSet = dbConnector.executeQuery(
				"select alpinist_id from " + DBTables.DB_ALPINISTS_TABLE
						+ " where alpinist_name=?", alpinistName);
		if (resultSet.next())
			return resultSet.getInt(1);
		return -1;
	}

	public int getEventId(String eventName) throws SQLException {
		ResultSet resultSet = dbConnector.executeQuery("select event_id from "
				+ DBTables.DB_EVENTS_TABLE + " where event_name='" + eventName
				+ "'");
		if (resultSet.next())
			return resultSet.getInt(1);
		return -1;
	}

	public int addAlpinistRow(String alpinistName, String alpinistCountry,
			String alpinistAddress) throws SQLException {
		int id = getAlpinistId(alpinistName), countryId, addressId;
		if (id == -1) {
			countryId = addCountryRow(alpinistCountry);
			addressId = addAddressRow(alpinistAddress);
			dbConnector.executeQuery("call add_alpinist('" + alpinistName
					+ "'," + countryId + "," + addressId + ")");
			return getRockId(alpinistName);
		} else
			return id;
	}

	public void addEventMember(String eventName, String memberName) {

	}

	public boolean isAlpinistAppended(int eventId, String alpinistName)
			throws SQLException {
		int alpinistId = getAlpinistId(alpinistName);
		ResultSet resultSet = dbConnector.executeQuery("select * from "
				+ DBTables.DB_ALP_HAS_EVNT_TABLE + " where event_id=" + eventId
				+ " AND alpinist_id=" + alpinistId);
		return resultSet.next();
	}

	public void addEventAlpinist(int eventId, String memberName)
			throws SQLException {
		if (!isAlpinistAppended(eventId, memberName))
			dbConnector.executeQuery("call add_alpinist_event("
					+ getAlpinistId(memberName) + "," + eventId + ")");
	}

	public void addEvent(String eventName, Object[] members,
			String eventStartDate, String eventStopDate, String eventRockName)
			throws SQLException {
		int eventId;
		eventId = addEventRow(eventName, eventStartDate, eventStopDate,
				eventRockName);
		for (Object member : members) {
			this.addEventAlpinist(eventId, (String) member);
		}
	}

	public int addRockRow(String rockName, int rockAltitude,
			String rockCountryName) throws SQLException {
		int id = getRockId(rockName), countryId = addCountryRow(rockCountryName);

		if (id == -1) {
			dbConnector.executeQuery("call add_rock('" + rockName + "',"
					+ rockAltitude + "," + countryId + ")");
			return getRockId(rockName);
		} else {
			task_3(id, rockAltitude, countryId);
			return id;
		}
	}

	public DBConnector getDbConnector() {
		return dbConnector;
	}
}
