package com.auvn.pbz2_app.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnector {
	private Connection dbConnection;
//	private String host = "jdbc:mysql://localhost/laba2_pbz";
	private String host = "jdbc:mysql://10.0.0.12/laba2_pbz";
	private String user = "root", password = "have2bnew";
	private String mysqlDriver = "com.mysql.jdbc.Driver";

	public DBConnector() {
		try {
			Class.forName(mysqlDriver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void connectToDB() throws SQLException {
		this.dbConnection = DriverManager.getConnection(host, user, password);
		System.out.println("Connected to: " + dbConnection.getCatalog());
	}

	public ResultSet executeQuery(String queryStr, String... strs)
			throws SQLException {
		System.out.println(queryStr);
		PreparedStatement preparedStatement = this.dbConnection
				.prepareStatement(queryStr);
		for (int i = 0; i < strs.length; i++) {
			preparedStatement.setString(i + 1, strs[i]);
		}
		return preparedStatement.executeQuery();
	}

}
