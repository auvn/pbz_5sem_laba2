package com.auvn.pbz2_app.db;

public interface DBTables {
	public static final String DB_ALPINISTS_TABLE = "alpinists";
	public static final String DB_ADDRESSES_TABLE = "addresses";
	public static final String DB_COUNTRIES_TABLE = "countries";
	public static final String DB_ROCKS_TABLE = "rocks";
	public static final String DB_EVENTS_TABLE = "events";
	public static final String DB_ALP_HAS_EVNT_TABLE = "alpinist_has_event";
}
