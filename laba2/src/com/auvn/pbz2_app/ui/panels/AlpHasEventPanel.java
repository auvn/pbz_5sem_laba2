package com.auvn.pbz2_app.ui.panels;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.auvn.pbz2_app.db.DBTables;

public class AlpHasEventPanel extends JScrollPane implements
		TablePanelInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = -78260898679091848L;
	private JTable alpHasEventTable;

	public void fillDataTable(Object[] columns, Object[][] data) {

		alpHasEventTable = new JTable(data, columns);
		setViewportView(alpHasEventTable);
	}

	public String getDBTableName() {
		return DBTables.DB_ALP_HAS_EVNT_TABLE;
	}

	public JTable getDataTable() {
		return alpHasEventTable;
	}
	
	public JComponent getComponent() {
		return this;
	}
}
