package com.auvn.pbz2_app.ui.panels.actions;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;

import com.auvn.pbz2_app.db.DBAdaptor;
import com.auvn.pbz2_app.ui.dialogs.EventInfoDialog;

public class EventNameFieldActions implements ItemListener {
	private DBAdaptor dbAdaptor;
	private EventInfoDialog eventInfoDialog;

	public EventNameFieldActions(EventInfoDialog eventInfoDialog,
			DBAdaptor dbAdaptor) {
		this.dbAdaptor = dbAdaptor;
		this.eventInfoDialog = eventInfoDialog;
	}


	public void itemStateChanged(ItemEvent arg0) {
		checkEvent();
	}

	private void checkEvent() {
		try {
			if (dbAdaptor.getEventId((String) eventInfoDialog
					.getEventNameComboBox().getSelectedItem()) != -1)
				eventInfoDialog.getEventRocksComboBox().setEnabled(false);
			else
				eventInfoDialog.getEventRocksComboBox().setEnabled(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
