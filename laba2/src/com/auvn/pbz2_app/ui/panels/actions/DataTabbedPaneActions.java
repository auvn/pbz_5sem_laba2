package com.auvn.pbz2_app.ui.panels.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import javax.swing.JTabbedPane;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;

public class DataTabbedPaneActions implements MouseListener {
	private TablePanelsAdaptor tablePanelsAdaptor;

	public DataTabbedPaneActions(TablePanelsAdaptor tablePanelsAdaptor) {
		this.tablePanelsAdaptor = tablePanelsAdaptor;
	}

	public void mouseClicked(MouseEvent arg0) {

	}

	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseReleased(MouseEvent arg0) {
		JTabbedPane tabbedPane = (JTabbedPane) arg0.getSource();
		int selectedIndex = tabbedPane.getSelectedIndex();
		if (selectedIndex != -1)
			try {
				tablePanelsAdaptor.updateSelectedtable(tabbedPane
						.getTitleAt(selectedIndex));
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
}
