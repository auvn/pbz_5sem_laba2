package com.auvn.pbz2_app.ui.panels;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.auvn.pbz2_app.db.DBTables;

public class CountriesPanel extends JScrollPane implements TablePanelInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3522859061100993523L;
	private JTable countriesTable;

	public void fillDataTable(Object[] columns, Object[][] data) {
		countriesTable = new JTable(data, columns);
		setViewportView(countriesTable);
	}

	public String getDBTableName() {
		return DBTables.DB_COUNTRIES_TABLE;
	}

	public JTable getDataTable() {
		return countriesTable;
	}

	public JComponent getComponent() {
		return this;
	}

}
