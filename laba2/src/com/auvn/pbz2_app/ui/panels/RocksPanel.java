package com.auvn.pbz2_app.ui.panels;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.auvn.pbz2_app.db.DBTables;

public class RocksPanel extends JScrollPane implements TablePanelInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1201834821779735168L;
	private JTable rocksTable;

	public RocksPanel() {
		rocksTable = new JTable();
		setViewportView(rocksTable);
	}

	public void fillDataTable(Object[] columns, Object[][] data) {
		rocksTable = new JTable(data, columns);
		setViewportView(rocksTable);
	}

	public String getDBTableName() {
		return DBTables.DB_ROCKS_TABLE;
	}

	public JTable getDataTable() {
		return rocksTable;
	}

	public JComponent getComponent() {
		return this;
	}
}
