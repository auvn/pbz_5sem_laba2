package com.auvn.pbz2_app.ui.panels;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.auvn.pbz2_app.db.DBTables;

public class AddressesPanel extends JScrollPane implements TablePanelInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8305364854976242817L;
	private JTable addressesTable;

	public void fillDataTable(Object[] columns, Object[][] data) {
		addressesTable = new JTable(data, columns);
		setViewportView(addressesTable);
	}

	public String getDBTableName() {
		return DBTables.DB_ADDRESSES_TABLE;
	}

	public JTable getDataTable() {
		return addressesTable;
	}

	public JComponent getComponent() {
		return this;
	}
}
