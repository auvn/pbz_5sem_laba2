package com.auvn.pbz2_app.ui.panels;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class MainPanel extends JPanel {
	private static final long serialVersionUID = -8039213284332686139L;
	private JTabbedPane mainTabbedPane;

	public MainPanel() {
		setLayout(new BorderLayout());

		mainTabbedPane = new JTabbedPane();

		add(mainTabbedPane, BorderLayout.CENTER);
	}

	public void addTab(String name, JComponent component) {
		mainTabbedPane.add(name, component);
	}

	public JTabbedPane getMainTabbedPane() {
		return mainTabbedPane;
	}
	
}
