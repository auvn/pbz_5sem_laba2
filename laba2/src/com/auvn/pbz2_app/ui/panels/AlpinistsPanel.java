package com.auvn.pbz2_app.ui.panels;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.auvn.pbz2_app.db.DBTables;

public class AlpinistsPanel extends JScrollPane implements TablePanelInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = 858286059600292480L;
	private JTable alpinistsTable;

	public void fillDataTable(Object[] columns, Object[][] data) {

		alpinistsTable = new JTable(data, columns);
		setViewportView(alpinistsTable);
	}

	public String getDBTableName() {
		return DBTables.DB_ALPINISTS_TABLE;
	}

	public JTable getDataTable() {
		return alpinistsTable;
	}
	
	public JComponent getComponent() {
		return this;
	}
}
