package com.auvn.pbz2_app.ui.panels;

import javax.swing.JComponent;
import javax.swing.JTable;

public interface TablePanelInterface {
	public void fillDataTable(Object[] columns, Object[][] data);

	public String getDBTableName();

	public JTable getDataTable();

	public JComponent getComponent();
}
