package com.auvn.pbz2_app.ui.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TaskResultPanel extends JPanel implements TablePanelInterface {
	private static final long serialVersionUID = -3563146411317178743L;
	private JTable resultTable;
	private JScrollPane scrollPane;

	public TaskResultPanel() {
		setLayout(new BorderLayout());
		scrollPane = new JScrollPane();
	}

	public void fillDataTable(Object[] columns, Object[][] data) {
		resultTable = new JTable(data, columns);
		resultTable.setMinimumSize(new Dimension(300, 300));
		scrollPane.setViewportView(resultTable);
		remove(scrollPane);
		add(scrollPane, BorderLayout.CENTER);
	}

	public JComponent getComponent() {
		return this;
	}

	public JTable getDataTable() {
		return resultTable;
	}

	public String getDBTableName() {
		return "Results";
	}
}
