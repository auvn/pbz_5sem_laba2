package com.auvn.pbz2_app.ui.panels;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.auvn.pbz2_app.db.DBTables;

public class EventsPanel extends JScrollPane implements TablePanelInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1011831517673022327L;
	private JTable eventsTable;

	public EventsPanel() {
		eventsTable = new JTable();
		setViewportView(eventsTable);
	}

	public void fillDataTable(Object[] columns, Object[][] data) {
		eventsTable = new JTable(data, columns);
		setViewportView(eventsTable);
	}

	public String getDBTableName() {
		return DBTables.DB_EVENTS_TABLE;
	}

	public JTable getDataTable() {
		return eventsTable;
	}
	
	public JComponent getComponent() {
		return this;
	}
}
