package com.auvn.pbz2_app.ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import com.auvn.pbz2_app.ui.menus.MainMenuBar;
import com.auvn.pbz2_app.ui.panels.MainPanel;

public class MainFrame extends JFrame {

	private MainMenuBar mainMenuBar;
	private MainPanel mainPanel;

	private static final long serialVersionUID = -6605263912220028992L;

	public MainFrame() {
		setProperties();
	}

	public void setMainMenuBar(MainMenuBar mainMenuBar) {
		this.mainMenuBar = mainMenuBar;
		setJMenuBar(mainMenuBar);
	}

	public void setMainPanel(MainPanel mainPanel) {
		this.mainPanel = mainPanel;
		setContentPane(mainPanel);
	}

	private void setProperties() {
		setMinimumSize(new Dimension(500, 600));
		setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 2
				- getSize().width / 2, Toolkit.getDefaultToolkit()
				.getScreenSize().height / 2 - getSize().height / 2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public MainMenuBar getMainMenuBar() {
		return mainMenuBar;
	}

	public MainPanel getMainPanel() {
		return mainPanel;
	}

}
