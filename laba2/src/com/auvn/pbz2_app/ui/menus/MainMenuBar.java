package com.auvn.pbz2_app.ui.menus;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainMenuBar extends JMenuBar {
	private static final long serialVersionUID = -9187809308681799311L;

	private JMenu operationsMenu;
	private JMenu tasksMenu;

	private JMenuItem addAlpinistMenuItem;
	private JMenuItem addUpdateRockInfo;
	private JMenuItem addEventMenuItem;

	private JMenuItem task1Item;
	private JMenuItem task2Item;
	private JMenuItem task3Item;
	private JMenuItem task4Item;
	private JMenuItem task5Item;
	private JMenuItem task6Item;
	private JMenuItem task7Item;
	private JMenuItem task8Item;
	private JMenuItem task9Item;

	public MainMenuBar() {
		operationsMenu = new JMenu("Operations");
		tasksMenu = new JMenu("Tasks");

		addAlpinistMenuItem = new JMenuItem("Add Alpinist");
		addUpdateRockInfo = new JMenuItem("Add/Update rock info");
		addEventMenuItem = new JMenuItem("Add Event");

		task1Item = new JMenuItem("Task 1");
		task2Item = new JMenuItem("Task 2(Add rock)");
		task2Item.setEnabled(false);
		task3Item = new JMenuItem("Task 3");
		task3Item.setEnabled(false);
		task4Item = new JMenuItem("Task 4");
		task5Item = new JMenuItem("Task 5(Add alpinist)");
		task5Item.setEnabled(false);
		task6Item = new JMenuItem("Task 6");
		task7Item = new JMenuItem("Task 7");
		task8Item = new JMenuItem("Task 8(Add event)");
		task8Item.setEnabled(false);
		task9Item = new JMenuItem("Task 9");

		operationsMenu.add(addAlpinistMenuItem);
		operationsMenu.add(addEventMenuItem);
		 operationsMenu.add(addUpdateRockInfo);

		tasksMenu.add(task1Item);
		tasksMenu.add(task2Item);
		tasksMenu.add(task3Item);
		tasksMenu.add(task4Item);
		tasksMenu.add(task5Item);
		tasksMenu.add(task6Item);
		tasksMenu.add(task7Item);
		tasksMenu.add(task8Item);
		tasksMenu.add(task9Item);

		add(operationsMenu);
		add(tasksMenu);
	}

	public JMenu getOperationsMenu() {
		return operationsMenu;
	}

	public void setOperationsMenu(JMenu operationsMenu) {
		this.operationsMenu = operationsMenu;
	}

	public JMenuItem getAddAlpinistMenuItem() {
		return addAlpinistMenuItem;
	}

	public void setAddAlpinistMenuItem(JMenuItem addAlpinistMenuItem) {
		this.addAlpinistMenuItem = addAlpinistMenuItem;
	}

	

	public JMenuItem getAddUpdateRockInfo() {
		return addUpdateRockInfo;
	}

	public void setAddUpdateRockInfo(JMenuItem addUpdateRockInfo) {
		this.addUpdateRockInfo = addUpdateRockInfo;
	}

	public JMenuItem getAddEventMenuItem() {
		return addEventMenuItem;
	}

	public void setAddEventMenuItem(JMenuItem addEventMenuItem) {
		this.addEventMenuItem = addEventMenuItem;
	}

	public JMenuItem getTask1Item() {
		return task1Item;
	}

	public void setTask1Item(JMenuItem task1Item) {
		this.task1Item = task1Item;
	}

	public JMenuItem getTask2Item() {
		return task2Item;
	}

	public void setTask2Item(JMenuItem task2Item) {
		this.task2Item = task2Item;
	}

	public JMenuItem getTask3Item() {
		return task3Item;
	}

	public void setTask3Item(JMenuItem task3Item) {
		this.task3Item = task3Item;
	}

	public JMenuItem getTask4Item() {
		return task4Item;
	}

	public void setTask4Item(JMenuItem task4Item) {
		this.task4Item = task4Item;
	}

	public JMenuItem getTask5Item() {
		return task5Item;
	}

	public void setTask5Item(JMenuItem task5Item) {
		this.task5Item = task5Item;
	}

	public JMenuItem getTask6Item() {
		return task6Item;
	}

	public void setTask6Item(JMenuItem task6Item) {
		this.task6Item = task6Item;
	}

	public JMenuItem getTask7Item() {
		return task7Item;
	}

	public void setTask7Item(JMenuItem task7Item) {
		this.task7Item = task7Item;
	}

	public JMenuItem getTask8Item() {
		return task8Item;
	}

	public void setTask8Item(JMenuItem task8Item) {
		this.task8Item = task8Item;
	}

	public JMenuItem getTask9Item() {
		return task9Item;
	}

	public void setTask9Item(JMenuItem task9Item) {
		this.task9Item = task9Item;
	}

}
