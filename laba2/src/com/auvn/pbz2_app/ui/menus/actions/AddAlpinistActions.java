package com.auvn.pbz2_app.ui.menus.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.auvn.pbz2_app.db.DBAdaptor;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;
import com.auvn.pbz2_app.ui.dialogs.AlpinistInfoDialog;

public class AddAlpinistActions implements ActionListener {
	private AlpinistInfoDialog alpinistInfoDialog;
	private TablePanelsAdaptor tablePanelsAdaptor;
	private String newAlpinistName;
	private String newAlpinistCountryName;
	private String newAlpinistAddressName;
	private DBAdaptor dbAdaptor;

	public AddAlpinistActions(AlpinistInfoDialog alpinistInfoDialog,
			TablePanelsAdaptor tablePanelsAdaptor, DBAdaptor dbAdaptor) {
		this.alpinistInfoDialog = alpinistInfoDialog;
		this.tablePanelsAdaptor = tablePanelsAdaptor;
		this.dbAdaptor = dbAdaptor;
	}

	public void actionPerformed(ActionEvent arg0) {
		int retStatus = alpinistInfoDialog.show(tablePanelsAdaptor);
		if (retStatus == JOptionPane.OK_OPTION) {
			newAlpinistName = alpinistInfoDialog.getAlpinistNameTextField()
					.getText();
			newAlpinistCountryName = (String) alpinistInfoDialog
					.getCountriesComboBox().getSelectedItem();
			newAlpinistAddressName = (String) alpinistInfoDialog
					.getAddressesComboBox().getSelectedItem();

			if (newAlpinistName.length() == 0
					|| newAlpinistCountryName.length() == 0
					|| newAlpinistAddressName.length() == 0)
				actionPerformed(arg0);
			else
				try {
					dbAdaptor.addAlpinistRow(newAlpinistName,
							newAlpinistCountryName, newAlpinistAddressName);
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
