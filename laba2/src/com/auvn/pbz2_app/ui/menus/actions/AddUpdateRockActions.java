package com.auvn.pbz2_app.ui.menus.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.auvn.pbz2_app.db.DBAdaptor;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;
import com.auvn.pbz2_app.ui.dialogs.RockInfoDialog;

public class AddUpdateRockActions implements ActionListener {

	private RockInfoDialog rockInfoDialog;
	private TablePanelsAdaptor tablePanelsAdaptor;
	private DBAdaptor dbAdaptor;

	private String newRockName;
	private String newRockAltitudeStr;
//	private int newRockAltitude;
	private String newRockCountry;

	public AddUpdateRockActions(RockInfoDialog rockInfoDialog,
			TablePanelsAdaptor tablePanelsAdaptor, DBAdaptor dbAdaptor) {
		this.rockInfoDialog = rockInfoDialog;
		this.tablePanelsAdaptor = tablePanelsAdaptor;
		this.dbAdaptor = dbAdaptor;
	}

	public void actionPerformed(ActionEvent arg0) {

		int retStatus = rockInfoDialog.show(tablePanelsAdaptor);
		if (retStatus == JOptionPane.OK_OPTION) {
			if (rockInfoDialog.getRockNameTextField().isVisible())
				newRockName = (String) rockInfoDialog.getRockNameTextField()
						.getText();
			else if (rockInfoDialog.getRockCountriesComboBox().isVisible())
				newRockName = (String) rockInfoDialog.getRockNamesComboBox()
						.getSelectedItem();
			newRockAltitudeStr = (String) rockInfoDialog
					.getRockAltitudeTextField().getText();
			newRockCountry = (String) rockInfoDialog.getRockCountriesComboBox()
					.getSelectedItem();
			if (newRockName == null || newRockCountry == null
					|| newRockAltitudeStr.length() == 0)
				actionPerformed(arg0);
			else {
				try {
					dbAdaptor.addRockRow(newRockName,
							Integer.parseInt(newRockAltitudeStr),
							newRockCountry);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				rockInfoDialog.setDialogType(0);
			}
		}
	}
}
