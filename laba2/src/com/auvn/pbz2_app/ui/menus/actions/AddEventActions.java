package com.auvn.pbz2_app.ui.menus.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.auvn.pbz2_app.db.DBAdaptor;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;
import com.auvn.pbz2_app.ui.dialogs.EventInfoDialog;
import com.auvn.pbz2_app.ui.dialogs.RockInfoDialog;

public class AddEventActions implements ActionListener {
	private EventInfoDialog eventInfoDialog;
	private TablePanelsAdaptor tablePanelsAdaptor;
	private RockInfoDialog rockInfoDialog;

	private Object[] newEventMembers;
	private String newEventStartDate;
	private String newEventStopDate;
	private String newEventName;
	private String newEventRock;

	private DBAdaptor dbAdaptor;

	private AddUpdateRockActions addRockActions;

	public AddEventActions(EventInfoDialog eventInfoDialog,
			TablePanelsAdaptor tablePanelsAdaptor, DBAdaptor dbAdaptor,
			AddUpdateRockActions addRockActions, RockInfoDialog rockInfoDialog) {
		this.eventInfoDialog = eventInfoDialog;
		this.tablePanelsAdaptor = tablePanelsAdaptor;
		this.dbAdaptor = dbAdaptor;
		this.addRockActions = addRockActions;
		this.rockInfoDialog = rockInfoDialog;
	}

	public void actionPerformed(ActionEvent arg0) {
		int retStatus = eventInfoDialog.show(tablePanelsAdaptor);
		if (retStatus == JOptionPane.OK_OPTION) {
			newEventMembers = eventInfoDialog.getMembersList()
					.getSelectedValuesList().toArray();
			newEventName = (String) eventInfoDialog.getEventNameComboBox()
					.getSelectedItem();
			newEventStartDate = eventInfoDialog.getEventStartDateTextField()
					.getText();
			newEventStopDate = eventInfoDialog.getEventStopDateTextField()
					.getText();
			newEventRock = (String) eventInfoDialog.getEventRocksComboBox()
					.getSelectedItem();

			if (newEventName == null || newEventMembers.length == 0
					|| newEventRock == null)
				actionPerformed(arg0);
			else
				try {
					if (dbAdaptor.getRockId(newEventRock) == -1) {
						rockInfoDialog.setDialogType(1);
						rockInfoDialog.getRockNameTextField().setText(
								newEventRock);
						addRockActions.actionPerformed(arg0);
					}

					dbAdaptor.addEvent(newEventName, newEventMembers,
							newEventStartDate, newEventStopDate, newEventRock);
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
