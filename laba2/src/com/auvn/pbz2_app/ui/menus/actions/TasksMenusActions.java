package com.auvn.pbz2_app.ui.menus.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.auvn.pbz2_app.db.DBAdaptor;
import com.auvn.pbz2_app.ui.MainFrame;
import com.auvn.pbz2_app.ui.ResultsFrame;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;
import com.auvn.pbz2_app.ui.dialogs.Task47Dialog;
import com.auvn.pbz2_app.ui.menus.MainMenuBar;
import com.auvn.pbz2_app.ui.panels.TaskResultPanel;

public class TasksMenusActions implements ActionListener {
	private MainMenuBar mainMenuBar;
	private TaskResultPanel taskResultPanel;
	private DBAdaptor dbAdaptor;
	// private TablePanelsAdaptor tablePanelsAdaptor;
	private ResultsFrame resultsFrame;
	private Task47Dialog task47Dialog;
	private ResultSet resultSet;

	public TasksMenusActions(MainFrame mainFrame,
			TaskResultPanel taskResultPanel, DBAdaptor dbAdaptor,
			TablePanelsAdaptor tablePanelsAdaptor, ResultsFrame resultsFrame,
			Task47Dialog task4Dialog) {
		this.mainMenuBar = mainFrame.getMainMenuBar();
		this.taskResultPanel = taskResultPanel;
		this.dbAdaptor = dbAdaptor;
		// this.tablePanelsAdaptor = tablePanelsAdaptor;
		this.resultsFrame = resultsFrame;
		this.task47Dialog = task4Dialog;
	}

	public void actionPerformed(ActionEvent arg0) {
		try {
			resultSet = null;
			if (arg0.getSource() == mainMenuBar.getTask1Item()) {
				resultSet = dbAdaptor.task_1();

			} else if (arg0.getSource() == mainMenuBar.getTask2Item()) {
				System.out.println("task2");
			} else if (arg0.getSource() == mainMenuBar.getTask3Item()) {
				System.out.println("task3");
			} else if (arg0.getSource() == mainMenuBar.getTask4Item()) {
				if (task47Dialog.show() == JOptionPane.OK_OPTION) {
					resultSet = dbAdaptor.task_4(task47Dialog
							.getDateFromTextField().getText(), task47Dialog
							.getDateTillTextField().getText());
				}
			} else if (arg0.getSource() == mainMenuBar.getTask5Item()) {
				System.out.println("task5");
			} else if (arg0.getSource() == mainMenuBar.getTask6Item()) {
				resultSet = dbAdaptor.task_6();

			} else if (arg0.getSource() == mainMenuBar.getTask7Item()) {
				if (task47Dialog.show() == JOptionPane.OK_OPTION) {
					resultSet = dbAdaptor.task_7(task47Dialog
							.getDateFromTextField().getText(), task47Dialog
							.getDateTillTextField().getText());
				}
			} else if (arg0.getSource() == mainMenuBar.getTask8Item()) {
				System.out.println("task8");
			} else if (arg0.getSource() == mainMenuBar.getTask9Item()) {
				resultSet = dbAdaptor.task_9();
			}
			if (resultSet != null) {
				taskResultPanel.fillDataTable(dbAdaptor.getColumns(resultSet),
						dbAdaptor.getData(resultSet));
				resultsFrame.setVisible(true);
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}
}
