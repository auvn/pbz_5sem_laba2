package com.auvn.pbz2_app.ui;

import javax.swing.JFrame;

import com.auvn.pbz2_app.ui.panels.TaskResultPanel;

public class ResultsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 893284501065811953L;

	public ResultsFrame(TaskResultPanel taskResultPanel) {
		setContentPane(taskResultPanel);
		setProperties();
	}

	private void setProperties() {
		setSize(300, 300);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
//		setVisible(true);
	}
}
