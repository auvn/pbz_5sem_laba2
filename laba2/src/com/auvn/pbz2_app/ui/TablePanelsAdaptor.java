package com.auvn.pbz2_app.ui;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.auvn.pbz2_app.db.DBAdaptor;
import com.auvn.pbz2_app.db.DBConnector;
import com.auvn.pbz2_app.ui.panels.MainPanel;
import com.auvn.pbz2_app.ui.panels.TablePanelInterface;

public class TablePanelsAdaptor {
	private Map<String, TablePanelInterface> tablesMap;
	private DBConnector dbConnector;
	private DBAdaptor dbAdaptor;

	public TablePanelsAdaptor(DBAdaptor dbAdaptor) {
		this.dbAdaptor = dbAdaptor;
		this.dbConnector = dbAdaptor.getDbConnector();
		tablesMap = new HashMap<String, TablePanelInterface>();
	}

	public void addDBTableDataPanel(TablePanelInterface tablePanel) {
		tablesMap.put(tablePanel.getDBTableName(), tablePanel);
	}

	public void addTabs(MainPanel mainPanel) {
		for (String key : this.tablesMap.keySet()) {
			mainPanel.addTab(key, tablesMap.get(key).getComponent());
		}
	}

	public void updateDataTable(TablePanelInterface tablePanelInterface)
			throws SQLException {
		ResultSet resultSet = dbConnector.executeQuery("select * from "
				+ tablePanelInterface.getDBTableName());
		tablePanelInterface.fillDataTable(dbAdaptor.getColumns(resultSet),
				dbAdaptor.getData(resultSet));
	}

	public void updateSelectedtable(String dbTableName) throws SQLException {
		updateDataTable(this.tablesMap.get(dbTableName));
	}

	public void updateAllTablesContent() {
		for (String panel : this.tablesMap.keySet()) {
			try {
				updateDataTable(this.tablesMap.get(panel));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public TablePanelInterface getTablePanelInterface(String dbTableName) {
		return tablesMap.get(dbTableName);
	}
}
