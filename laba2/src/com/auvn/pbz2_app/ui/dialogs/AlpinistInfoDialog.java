package com.auvn.pbz2_app.ui.dialogs;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.auvn.pbz2_app.db.DBTables;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;

public class AlpinistInfoDialog {

	private Object[] dialogObjects;
	private JLabel alpinistNameLabel;
	private JTextField alpinistNameTextField;

	private JLabel alpinistCountryLabel;
	private JComboBox<Object> countriesComboBox;

	private JLabel alpinistAddressLabel;
	private JComboBox<Object> addressesComboBox;

	public AlpinistInfoDialog() {

		alpinistNameLabel = new JLabel("Alpinist name:");
		alpinistNameTextField = new JTextField();

		alpinistCountryLabel = new JLabel("Country:");
		countriesComboBox = new JComboBox<Object>();
		countriesComboBox.setEditable(true);

		alpinistAddressLabel = new JLabel("Address:");
		addressesComboBox = new JComboBox<Object>();
		addressesComboBox.setEditable(true);

		dialogObjects = new Object[] { alpinistNameLabel,
				alpinistNameTextField, alpinistCountryLabel, countriesComboBox,
				alpinistAddressLabel, addressesComboBox };
	}

	public int show(TablePanelsAdaptor tablePanelsAdaptor) {
		JTable countriesTable = tablePanelsAdaptor.getTablePanelInterface(
				DBTables.DB_COUNTRIES_TABLE).getDataTable();
		JTable addressesTable = tablePanelsAdaptor.getTablePanelInterface(
				DBTables.DB_ADDRESSES_TABLE).getDataTable();
		countriesComboBox.removeAllItems();
		addressesComboBox.removeAllItems();
		for (int i = 0; i < countriesTable.getRowCount(); i++) {
			countriesComboBox.addItem(countriesTable.getValueAt(i, 1));
		}
		for (int i = 0; i < addressesTable.getRowCount(); i++) {
			addressesComboBox.addItem(addressesTable.getValueAt(i, 1));
		}

		return JOptionPane.showConfirmDialog(null, dialogObjects,
				"New Alpinist", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
	}

	public JTextField getAlpinistNameTextField() {
		return alpinistNameTextField;
	}

	public JComboBox<Object> getCountriesComboBox() {
		return countriesComboBox;
	}

	public void setCountriesComboBox(JComboBox<Object> countriesComboBox) {
		this.countriesComboBox = countriesComboBox;
	}

	public JComboBox<Object> getAddressesComboBox() {
		return addressesComboBox;
	}

	public void setAddressesComboBox(JComboBox<Object> addressesComboBox) {
		this.addressesComboBox = addressesComboBox;
	}

}
