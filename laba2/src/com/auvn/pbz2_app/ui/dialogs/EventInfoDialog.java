package com.auvn.pbz2_app.ui.dialogs;

import java.awt.Dimension;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.auvn.pbz2_app.db.DBTables;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;

public class EventInfoDialog {
	private Object[] dialogObjects;
	private JLabel eventNameLabel;
	private JComboBox<Object> eventNameComboBox;

	private JLabel eventStartDateLabel;
	private JTextField eventStartDateTextField;

	private JLabel eventStopDateLabel;
	private JTextField eventStopDateTextField;

	private JLabel membersLabel;
	private JList<Object> membersList;
	private DefaultListModel<Object> membersListModel;
	private JScrollPane membersListScrollPane;

	private JLabel eventRockNameLabel;
	private JComboBox<Object> eventRocksComboBox;

	public EventInfoDialog() {
		eventNameLabel = new JLabel("Event name:");
		eventNameComboBox = new JComboBox<Object>();
		eventNameComboBox.setEditable(true);

		eventStartDateLabel = new JLabel("Event start date:");
		eventStartDateTextField = new JTextField("2012-12-1");

		eventStopDateLabel = new JLabel("Event stop date:");
		eventStopDateTextField = new JTextField("2012-12-2");

		membersLabel = new JLabel("Members");
		membersList = new JList<Object>(
				membersListModel = new DefaultListModel<>());
		membersListScrollPane = new JScrollPane(membersList);
		membersListScrollPane.setMinimumSize(new Dimension(0, 100));

		eventRockNameLabel = new JLabel("Event rock name:");
		eventRocksComboBox = new JComboBox<Object>();
		eventRocksComboBox.setEditable(true);

		dialogObjects = new Object[] { eventNameLabel, eventNameComboBox,
				eventStartDateLabel, eventStartDateTextField,
				eventStopDateLabel, eventStopDateTextField, membersLabel,
				membersListScrollPane, eventRockNameLabel, eventRocksComboBox };
	}

	public int show(TablePanelsAdaptor tablePanelsAdaptor) {
		JTable eventsTable = tablePanelsAdaptor.getTablePanelInterface(
				DBTables.DB_EVENTS_TABLE).getDataTable();

		JTable alpinistsTable = tablePanelsAdaptor.getTablePanelInterface(
				DBTables.DB_ALPINISTS_TABLE).getDataTable();

		JTable rocksTable = tablePanelsAdaptor.getTablePanelInterface(
				DBTables.DB_ROCKS_TABLE).getDataTable();

		eventNameComboBox.removeAllItems();

		for (int i = 0; i < eventsTable.getRowCount(); i++)
			eventNameComboBox.addItem(eventsTable.getValueAt(i, 1));

		membersListModel.setSize(0);
		for (int i = 0; i < alpinistsTable.getRowCount(); i++)
			membersListModel.addElement(alpinistsTable.getValueAt(i, 1));

		eventRocksComboBox.removeAllItems();
		for (int i = 0; i < rocksTable.getRowCount(); i++)
			eventRocksComboBox.addItem(rocksTable.getValueAt(i, 1));

		return JOptionPane.showConfirmDialog(null, dialogObjects,
				"Event info:", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
	}

	public JComboBox<Object> getEventNameComboBox() {
		return eventNameComboBox;
	}

	public void setEventNameComboBox(JComboBox<Object> eventNameComboBox) {
		this.eventNameComboBox = eventNameComboBox;
	}

	public JTextField getEventStartDateTextField() {
		return eventStartDateTextField;
	}

	public void setEventStartDateTextField(JTextField eventStartDateTextField) {
		this.eventStartDateTextField = eventStartDateTextField;
	}

	public JTextField getEventStopDateTextField() {
		return eventStopDateTextField;
	}

	public void setEventStopDateTextField(JTextField eventStopDateTextField) {
		this.eventStopDateTextField = eventStopDateTextField;
	}

	public JList<Object> getMembersList() {
		return membersList;
	}

	public void setMembersList(JList<Object> membersList) {
		this.membersList = membersList;
	}

	public DefaultListModel<Object> getMembersListModel() {
		return membersListModel;
	}

	public void setMembersListModel(DefaultListModel<Object> membersListModel) {
		this.membersListModel = membersListModel;
	}

	public JComboBox<Object> getEventRocksComboBox() {
		return eventRocksComboBox;
	}

	public void setEventRocksComboBox(JComboBox<Object> eventRocksComboBox) {
		this.eventRocksComboBox = eventRocksComboBox;
	}

}
