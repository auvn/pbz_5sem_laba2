package com.auvn.pbz2_app.ui.dialogs;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Task47Dialog {

	private Object[] dialogObjects;

	private JLabel dateFromLabel;
	private JTextField dateFromTextField;

	private JLabel dateTillLabel;
	private JTextField dateTillTextField;

	public Task47Dialog() {

		dateFromLabel = new JLabel("Date from:");
		dateFromTextField = new JTextField("2012-10-10");

		dateTillLabel = new JLabel("Date till:");
		dateTillTextField = new JTextField("2012-11-10");

		dialogObjects = new Object[] { dateFromLabel, dateFromTextField,
				dateTillLabel, dateTillTextField };
	}

	public int show() {
		return JOptionPane.showConfirmDialog(null, dialogObjects,
				"Task 4 dates:", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
	}

	public JTextField getDateFromTextField() {
		return dateFromTextField;
	}

	public void setDateFromTextField(JTextField dateFromTextField) {
		this.dateFromTextField = dateFromTextField;
	}

	public JTextField getDateTillTextField() {
		return dateTillTextField;
	}

	public void setDateTillTextField(JTextField dateTillTextField) {
		this.dateTillTextField = dateTillTextField;
	}
	
}
