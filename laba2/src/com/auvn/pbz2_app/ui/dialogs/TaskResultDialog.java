package com.auvn.pbz2_app.ui.dialogs;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.auvn.pbz2_app.ui.panels.TaskResultPanel;

public class TaskResultDialog {
	private JLabel taskResultsLabel;
	private TaskResultPanel taskResultPanel;
	private Object[] dialogObjects;

	// private TablePanelsAdaptor tablePanelsAdaptor;

	public TaskResultDialog(TaskResultPanel taskResultPanel) {
		this.taskResultPanel = taskResultPanel;
		taskResultsLabel = new JLabel("Result table:");
		dialogObjects = new Object[] { taskResultsLabel, taskResultPanel };
	}

	public int show() {
		taskResultPanel.setMinimumSize(new Dimension(300, 300));
		return JOptionPane.showConfirmDialog(null, dialogObjects, "Rock info:",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	}
}
