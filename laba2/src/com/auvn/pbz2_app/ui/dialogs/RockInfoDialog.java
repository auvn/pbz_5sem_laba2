package com.auvn.pbz2_app.ui.dialogs;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.auvn.pbz2_app.db.DBTables;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;

public class RockInfoDialog {
	private Object[] dialogObjects;
	private JLabel rockNameLabel;
	private JTextField rockNameTextField;
	private JComboBox<Object> rockNamesComboBox;

	private JLabel rockAltitudeLabel;
	private JTextField rockAltitudeTextField;

	private JLabel rockCountryLabel;
	private JComboBox<Object> rockCountriesComboBox;

	private int dialogType = 0;

	public RockInfoDialog() {
		rockNameLabel = new JLabel("Rock name:");
		rockNameTextField = new JTextField("Rock");
		rockNamesComboBox = new JComboBox<Object>();
		rockNamesComboBox.setVisible(false);
		rockNamesComboBox.setEditable(true);

		rockAltitudeLabel = new JLabel("Rock altitude:");
		rockAltitudeTextField = new JTextField();

		rockCountryLabel = new JLabel("Rock country:");
		rockCountriesComboBox = new JComboBox<Object>();
		rockCountriesComboBox.setEditable(true);

		dialogObjects = new Object[] { rockNameLabel, rockNameTextField,
				rockNamesComboBox, rockAltitudeLabel, rockAltitudeTextField,
				rockCountryLabel, rockCountriesComboBox };
	}

	public int show(TablePanelsAdaptor tablePanelsAdaptor) {
		JTable countriesTable = tablePanelsAdaptor.getTablePanelInterface(
				DBTables.DB_COUNTRIES_TABLE).getDataTable();

		rockCountriesComboBox.removeAllItems();
		for (int i = 0; i < countriesTable.getRowCount(); i++) {
			rockCountriesComboBox.addItem(countriesTable.getValueAt(i, 1));
		}

		if (dialogType != 1) {
			JTable rocksTable = tablePanelsAdaptor.getTablePanelInterface(
					DBTables.DB_ROCKS_TABLE).getDataTable();
			rockNamesComboBox.removeAllItems();
			for (int i = 0; i < rocksTable.getRowCount(); i++) {
				rockNamesComboBox.addItem(rocksTable.getValueAt(i, 1));
			}
			rockNamesComboBox.setVisible(true);
			rockNameTextField.setVisible(false);
		} else {
			rockNamesComboBox.setVisible(false);
			rockNameTextField.setVisible(true);
		}
		return JOptionPane.showConfirmDialog(null, dialogObjects, "Rock info:",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	}

	public JComboBox<Object> getRockNamesComboBox() {
		return rockNamesComboBox;
	}

	public void setRockNamesComboBox(JComboBox<Object> rockNamesComboBox) {
		this.rockNamesComboBox = rockNamesComboBox;
	}

	public int getDialogType() {
		return dialogType;
	}

	public void setDialogType(int dialogType) {
		this.dialogType = dialogType;
	}

	public JTextField getRockAltitudeTextField() {
		return rockAltitudeTextField;
	}

	public void setRockAltitudeTextField(JTextField rockAltitudeTextField) {
		this.rockAltitudeTextField = rockAltitudeTextField;
	}

	public JComboBox<Object> getRockCountriesComboBox() {
		return rockCountriesComboBox;
	}

	public void setRockCountriesComboBox(JComboBox<Object> rockCountriesComboBox) {
		this.rockCountriesComboBox = rockCountriesComboBox;
	}

	public JLabel getRockNameLabel() {
		return rockNameLabel;
	}

	public JTextField getRockNameTextField() {
		return rockNameTextField;
	}

}
