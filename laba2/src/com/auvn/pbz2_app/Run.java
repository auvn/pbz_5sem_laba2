package com.auvn.pbz2_app;

import java.sql.SQLException;

import com.auvn.pbz2_app.db.DBAdaptor;
import com.auvn.pbz2_app.db.DBConnector;
import com.auvn.pbz2_app.ui.MainFrame;
import com.auvn.pbz2_app.ui.ResultsFrame;
import com.auvn.pbz2_app.ui.TablePanelsAdaptor;
import com.auvn.pbz2_app.ui.dialogs.AlpinistInfoDialog;
import com.auvn.pbz2_app.ui.dialogs.EventInfoDialog;
import com.auvn.pbz2_app.ui.dialogs.RockInfoDialog;
import com.auvn.pbz2_app.ui.dialogs.Task47Dialog;
import com.auvn.pbz2_app.ui.menus.MainMenuBar;
import com.auvn.pbz2_app.ui.menus.actions.AddAlpinistActions;
import com.auvn.pbz2_app.ui.menus.actions.AddEventActions;
import com.auvn.pbz2_app.ui.menus.actions.AddUpdateRockActions;
import com.auvn.pbz2_app.ui.menus.actions.TasksMenusActions;
import com.auvn.pbz2_app.ui.panels.AddressesPanel;
import com.auvn.pbz2_app.ui.panels.AlpHasEventPanel;
import com.auvn.pbz2_app.ui.panels.AlpinistsPanel;
import com.auvn.pbz2_app.ui.panels.CountriesPanel;
import com.auvn.pbz2_app.ui.panels.EventsPanel;
import com.auvn.pbz2_app.ui.panels.MainPanel;
import com.auvn.pbz2_app.ui.panels.RocksPanel;
import com.auvn.pbz2_app.ui.panels.TaskResultPanel;
import com.auvn.pbz2_app.ui.panels.actions.DataTabbedPaneActions;
import com.auvn.pbz2_app.ui.panels.actions.EventNameFieldActions;

public class Run {
	private static void init() throws SQLException {
		DBConnector dbConnector = new DBConnector();
		DBAdaptor dbAdaptor = new DBAdaptor(dbConnector);

		TablePanelsAdaptor tablePanelsAdaptor = new TablePanelsAdaptor(
				dbAdaptor);

		MainPanel mainPanel = new MainPanel();
		MainMenuBar mainMenuBar = new MainMenuBar();
		MainFrame mainFrame = new MainFrame();

		mainFrame.setMainPanel(mainPanel);
		mainFrame.setMainMenuBar(mainMenuBar);

		AlpinistInfoDialog alpinistInfoDialog = new AlpinistInfoDialog();
		EventInfoDialog eventInfoDialog = new EventInfoDialog();
		RockInfoDialog rockInfoDialog = new RockInfoDialog();
		Task47Dialog task4Dialog = new Task47Dialog();

		AlpinistsPanel alpinistsPanel = new AlpinistsPanel();
		AlpHasEventPanel alpHasEventPanel = new AlpHasEventPanel();
		AddressesPanel addressesPanel = new AddressesPanel();
		CountriesPanel countriesPanel = new CountriesPanel();
		RocksPanel rocksPanel = new RocksPanel();
		EventsPanel eventsPanel = new EventsPanel();
		TaskResultPanel taskResultPanel = new TaskResultPanel();

		ResultsFrame resultsFrame = new ResultsFrame(taskResultPanel);

		// TaskResultDialog taskResultDialog = new TaskResultDialog(
		// taskResultPanel);

		tablePanelsAdaptor.addDBTableDataPanel(alpinistsPanel);
		tablePanelsAdaptor.addDBTableDataPanel(alpHasEventPanel);
		tablePanelsAdaptor.addDBTableDataPanel(addressesPanel);
		tablePanelsAdaptor.addDBTableDataPanel(countriesPanel);
		tablePanelsAdaptor.addDBTableDataPanel(rocksPanel);
		tablePanelsAdaptor.addDBTableDataPanel(eventsPanel);

		tablePanelsAdaptor.addTabs(mainPanel);

		AddUpdateRockActions addRockActions = new AddUpdateRockActions(
				rockInfoDialog, tablePanelsAdaptor, dbAdaptor);

		mainMenuBar.getAddAlpinistMenuItem().addActionListener(
				new AddAlpinistActions(alpinistInfoDialog, tablePanelsAdaptor,
						dbAdaptor));
		mainMenuBar.getAddEventMenuItem().addActionListener(
				new AddEventActions(eventInfoDialog, tablePanelsAdaptor,
						dbAdaptor, addRockActions, rockInfoDialog));
		mainMenuBar.getAddUpdateRockInfo().addActionListener(
				new AddUpdateRockActions(rockInfoDialog, tablePanelsAdaptor,
						dbAdaptor));

		TasksMenusActions tasksMenusActions = new TasksMenusActions(mainFrame,
				taskResultPanel, dbAdaptor, tablePanelsAdaptor, resultsFrame,task4Dialog);

		mainMenuBar.getTask1Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask2Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask3Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask4Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask5Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask6Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask7Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask8Item().addActionListener(tasksMenusActions);
		mainMenuBar.getTask9Item().addActionListener(tasksMenusActions);

		mainPanel.getMainTabbedPane().addMouseListener(
				new DataTabbedPaneActions(tablePanelsAdaptor));

		eventInfoDialog.getEventNameComboBox().addItemListener(
				new EventNameFieldActions(eventInfoDialog, dbAdaptor));

		dbConnector.connectToDB();

		tablePanelsAdaptor.updateAllTablesContent();

		mainFrame.setVisible(true);
	}

	public static void main(String[] args) throws SQLException {
		init();

	}
}
