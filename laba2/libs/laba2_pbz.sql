SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `laba2_pbz` DEFAULT CHARACTER SET utf8 ;
USE `laba2_pbz` ;

-- -----------------------------------------------------
-- Table `laba2_pbz`.`addresses`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `laba2_pbz`.`addresses` (
  `address_id` INT NOT NULL AUTO_INCREMENT ,
  `address_info` VARCHAR(45) NULL ,
  PRIMARY KEY (`address_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laba2_pbz`.`countries`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `laba2_pbz`.`countries` (
  `country_id` INT NOT NULL AUTO_INCREMENT ,
  `country_name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`country_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laba2_pbz`.`alpinists`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `laba2_pbz`.`alpinists` (
  `alpinist_id` INT NOT NULL AUTO_INCREMENT ,
  `alpinist_name` VARCHAR(45) NOT NULL ,
  `address_id` INT NOT NULL ,
  `country_id` INT NOT NULL ,
  PRIMARY KEY (`alpinist_id`) ,
  INDEX `fk_alpinist_address1_idx` (`address_id` ASC) ,
  INDEX `fk_alpinist_country1_idx` (`country_id` ASC) ,
  CONSTRAINT `fk_alpinist_address1`
    FOREIGN KEY (`address_id` )
    REFERENCES `laba2_pbz`.`addresses` (`address_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alpinist_country1`
    FOREIGN KEY (`country_id` )
    REFERENCES `laba2_pbz`.`countries` (`country_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laba2_pbz`.`rocks`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `laba2_pbz`.`rocks` (
  `rock_id` INT NOT NULL AUTO_INCREMENT ,
  `rock_name` VARCHAR(45) NOT NULL ,
  `altitude` INT NOT NULL ,
  `is_visited` TINYINT(1) NOT NULL DEFAULT false ,
  `country_id` INT NOT NULL ,
  PRIMARY KEY (`rock_id`) ,
  INDEX `fk_rock_country1_idx` (`country_id` ASC) ,
  CONSTRAINT `fk_rock_country1`
    FOREIGN KEY (`country_id` )
    REFERENCES `laba2_pbz`.`countries` (`country_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laba2_pbz`.`events`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `laba2_pbz`.`events` (
  `event_id` INT NOT NULL AUTO_INCREMENT ,
  `event_name` VARCHAR(45) NOT NULL ,
  `event_start` DATE NOT NULL ,
  `event_end` DATE NOT NULL ,
  `rock_id` INT NOT NULL ,
  PRIMARY KEY (`event_id`) ,
  INDEX `fk_events_rocks1_idx` (`rock_id` ASC) ,
  CONSTRAINT `fk_events_rocks1`
    FOREIGN KEY (`rock_id` )
    REFERENCES `laba2_pbz`.`rocks` (`rock_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laba2_pbz`.`alpinist_has_event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `laba2_pbz`.`alpinist_has_event` (
  `alpinist_id` INT NOT NULL ,
  `event_id` INT NOT NULL ,
  PRIMARY KEY (`alpinist_id`, `event_id`) ,
  INDEX `fk_alpinist_has_event_event1_idx` (`event_id` ASC) ,
  INDEX `fk_alpinist_has_event_alpinist1_idx` (`alpinist_id` ASC) ,
  CONSTRAINT `fk_alpinist_has_event_alpinist1`
    FOREIGN KEY (`alpinist_id` )
    REFERENCES `laba2_pbz`.`alpinists` (`alpinist_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alpinist_has_event_event1`
    FOREIGN KEY (`event_id` )
    REFERENCES `laba2_pbz`.`events` (`event_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- procedure update_rock
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`update_rock` (IN i_rock_id INT, IN new_rock_name VARCHAR(45),IN new_altitude INT, IN i_is_visited BOOL,IN new_country_id INT, IN new_address_id INT)
BEGIN
	UPDATE rocks SET rock_name=new_rock_name AND altitude=new_altitude AND is_visited=i_is_visited AND country_id=new_country_id AND address_id=new_address_id WHERE rock_id=i_rock_id;
END $$

DELIMITER ;

-- -----------------------------------------------------
-- procedure add_rock
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`add_rock` (IN new_rock_name VARCHAR(45),IN new_altitude INT,IN new_country_id INT)
BEGIN
	INSERT INTO rocks (rock_name,altitude,country_id) VALUES (new_rock_name,new_altitude,new_country_id);
END $$

DELIMITER ;

-- -----------------------------------------------------
-- procedure add_address
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`add_address` (IN new_address_info VARCHAR(45))
BEGIN
	INSERT INTO addresses (address_info) VALUES (new_address_info);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure add_country
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`add_country` (IN new_country_name VARCHAR(45))
BEGIN
	INSERT INTO countries (country_name) VALUES (new_country_name);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure add_event
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`add_event` (IN new_event_name VARCHAR(45), IN new_event_start DATE, IN new_event_end DATE, IN rock_id INT)
BEGIN
	INSERT INTO events (event_name,event_start,event_end,rock_id) VALUES (new_event_name,new_event_start,new_event_end,rock_id);
	UPDATE rocks SET rocks.is_visited=1 WHERE rocks.rock_id=rock_id;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_1
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_1` ()
BEGIN
	SELECT * FROM (rocks INNER JOIN events USING(`event_id`) ORDER BY `event_start`);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_2
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_2` (IN new_rock_name VARCHAR(45),IN new_altitude INT,IN new_country_id INT)
BEGIN
	CALL `laba2_pbz`.`add_rock`(new_rock_name ,new_altitude, new_country_id);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_3
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_3` (IN rock_id INT,  IN new_altitude INT, IN new_country_id INT)
BEGIN
	UPDATE rocks SET rocks.altitude=new_altitude AND rocks.country_id=new_country_id WHERE rocks.rock_id=rock_id AND rocks.is_visited=0;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_4
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_4` (IN event_date_from DATE, IN event_date_till DATE)
BEGIN
	SELECT * FROM (alpinists INNER JOIN alpinist_has_event USING(`alpinist_id`)) JOIN addresses USING(`address_id`) JOIN countries USING(`country_id`) JOIN events USING(event_id) WHERE (event_start>=event_date_from AND event_start<=event_date_till) OR(event_end>=event_date_from AND event_end<=event_date_till);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure add_alpinist_event
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`add_alpinist_event` (IN alpinist_id INT, IN event_id INT)
BEGIN
	INSERT INTO alpinist_has_event (`alpinist_id`,`event_id`) VALUES (alpinist_id,event_id);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure add_alpinist
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`add_alpinist` (IN new_alpinist_name VARCHAR(45), IN new_country_id INT, IN new_address_id INT)
BEGIN
	INSERT INTO alpinists (`alpinist_name`,`country_id`,`address_id`) VALUES (new_alpinist_name,new_country_id, new_address_id);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_5
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_5` (IN new_alpinist_name VARCHAR(45), IN new_country_id INT, IN new_address_id INT)
BEGIN
	CALL `laba2_pbz`.`add_alpinist` (new_alpinist_name , new_country_id, new_address_id);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_6
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_6` ()
BEGIN
	SELECT rock_name,alpinist_name,count(alpinist_id) FROM rocks JOIN events USING(`rock_id`) JOIN alpinist_has_event USING(`event_id`) JOIN alpinists USING(alpinist_id) GROUP BY alpinist_id,rock_id ORDER BY rock_name;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_7
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_7` (IN event_date_from DATE, IN event_date_till DATE)
BEGIN
	SELECT * FROM events WHERE (event_start>=event_date_from AND event_start<=event_date_till) OR(event_end>=event_date_from AND event_end<=event_date_till) ;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_8
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_8` (IN new_event_name VARCHAR(45), IN rock_id INT, IN new_event_start DATE,IN new_event_end DATE)
BEGIN
	call laba2_pbz.add_event(new_event_name, new_event_start, new_event_end,rock_id);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure task_9
-- -----------------------------------------------------

DELIMITER $$
USE `laba2_pbz`$$
CREATE PROCEDURE `laba2_pbz`.`task_9` ()
BEGIN
	SELECT rock_name,count(alpinist_id) FROM rocks  JOIN events USING(`rock_id`) JOIN alpinist_has_event USING(`event_id`) GROUP BY rock_id;
END$$

DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
